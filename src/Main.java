import java.io.*;

public class Main {
    public static void main(String[] args){
        // Через консоль
        String line = null;
        try (BufferedReader consoleReader = new BufferedReader(new InputStreamReader(System.in))) {
            // Если необходимо несколько строчек, то можно сделать цикл do (<то, что сейчас в try>) while(line != null);
            line = consoleReader.readLine();
            checkWords(line);
        } catch(IOException e) {
            e.printStackTrace();
        }
        System.out.println("--------------------"); // для отделения вывода
        // Через файл
        File file = new File ("/C:/test/file.txt"); // В данной директории находится файл
        try (BufferedReader fileReader = new BufferedReader(new FileReader(file))) {
            while ((line = fileReader.readLine()) != null)
                checkWords(line);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static void checkWords(String line) {
        for (String word : line.split(" ")) {
            try {
                if (word.length() > 10)
                    throw new WordBiggerThanTenSymbolsException("Word: \"" + word + "\" is bigger than 10 symbols!");
            } catch (WordBiggerThanTenSymbolsException e){
                System.out.println(e.getMessage());
            }
        }
    }
}
