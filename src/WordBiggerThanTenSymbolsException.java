public class WordBiggerThanTenSymbolsException extends Exception {
    public WordBiggerThanTenSymbolsException() {
    }

    public WordBiggerThanTenSymbolsException(String message) {
        super(message);
    }

    public WordBiggerThanTenSymbolsException(String message, Throwable cause) {
        super(message, cause);
    }

    public WordBiggerThanTenSymbolsException(Throwable cause) {
        super(cause);
    }

    public WordBiggerThanTenSymbolsException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
